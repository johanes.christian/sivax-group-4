from django.urls import include, path

from dist.views import register_schedule, schedule_list

app_name='dist'

urlpatterns = [
    path('register-schedule/', register_schedule, name='register_schedule'),
    path('schedule-list/', schedule_list, name="schedule_list"),
]