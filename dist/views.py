from django.shortcuts import render

INSTITUTIONS = [
    ('D001', 'Rumah Sakit Sehat'),
    ('D002', 'Rumah Sakit Melati'),
    ('D003', 'Rumah Sakit Juanda'),
    ('D004', 'PT Sosru'),
    ('D005', 'Rumah Sakit Adi Mulya'),
    ('D006', 'Rumah Sakit Pertemanan'),
    ('D007', 'Rumah Sakit Sejahtera'),
    ('D008', 'PT Unilevel'),
    ('D009', 'PT Cipto Indonesia'),
    ('D010', 'Rumah Sakit BPJS'),
    ('D011', 'Rumah Sakit Jakarta'),
    ('D012', 'Rumah Sakit Pertama'),
    ('D013', 'PT Jakarta Raya'),
    ('D014', 'Rumah Sakit UI'),
    ('D015', 'Rumah Sakit Umum Negeri'),
    ('D016', 'Rumah Sakit Merdeka'),
    ('D017', 'PT Dua Harimau'),
    ('D018', 'PT Bintang Pratama'),
    ('D019', 'RSU Fatmawati'),
    ('D020', 'RS Pusat Pertamina'),
    ('D021', 'Puskesmas Pondok Indah'),
    ('D022', 'Puskesmas Sakit Tebet'),
    ('D023', 'Puskesmas Siaga Raya'),
    ('D024', 'Klinik Agung Jakarta'),
    ('D025', 'Klinik Indah Medika'),
]

LOCATIONS = [
    ('loc01', 'Rosemary Hospital Center', '349', '227', 'Mangga Bl', 'Rawa Buaya', 'Cengkareng', 'DKI Jakarta'),
    ('loc02', 'Memorial Medical Center', '316', '63', 'Merdeka', 'Tembalang', 'Tembalang', 'Semarang'),
    ('loc03', 'Pine Rest Community Hospital', '472', '52', 'Jendral Basuki', 'Joglo', 'Kembangan', 'DKI Jakarta'),
    ('loc04', 'Morningside Medical Center', '413', '210', 'Gatot Subroto', 'Kemanggisan', 'Palmerah', 'DKI Jakarta'),
    ('loc05', 'Heartstone Community Hospital', '301', '173', 'Psr Turi', 'Asemrowo', 'Asemrowo', 'Surabaya'),
    ('loc06', 'Pearl River Clinic', '417', '189', 'Psr Blok M', 'Glodok', 'Tamansari', 'DKI Jakarta'),
    ('loc07', 'Greengrass Medical Center', '133', '106', 'MH Thamrin', 'Banu', 'Malalayang', 'Manado'),
    ('loc08', 'Hot Springs Hospital Center', '414', '267', 'Jend Sudirman', 'Cikokol', 'Tangerang', 'Tangerang'),
    ('loc09', 'Fountain Valley Hospital', '452', '248', 'Aria Putra ', 'Klojen', 'Klojen', 'Malang'),
    ('loc010', 'Lowland General Hospital', '356', '39', 'Proy Senen ', 'Nusukan ', 'Banjarsari', 'Surakarta'),
]

SCHEDULE = [

]

def register_schedule(request):
    context = {'institutions':INSTITUTIONS, 'locations':LOCATIONS}
    return render(request, 'new-schedule-form.html', context)

def schedule_list(request):

    context = {}
    return render(request, 'schedule-list.html', context)